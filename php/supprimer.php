
<?php
    echo "Bonjour<br>";

    include_once 'connect.php';
        
    $pdo = connect();

    if (isset($_GET['id'])) {
        // Select the record that is going to be deleted
        $stmt = $pdo->prepare('SELECT * FROM t_ressource WHERE id_ressource = ?');
        $stmt->execute([$_GET['id']]);
        $ress= $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$ress) {
            exit('Pas de ressources avec cet ID!');
        }
      
        $stmt = $pdo->prepare('DELETE FROM t_ressource WHERE id_ressource = ?');
        $exec = $stmt->execute([$_GET['id']]);
        if($exec){
            echo 'Données supprimées dans t_ressource<br>';
          }else{
            echo 'Échec de l\'opération de suppression';
          }
            
        }
        else {
        exit('No ID specified!');
        }
        
        header("Refresh: 2;URL=".ROOTSERVER."/accueil");
  
?>