<?php


if(isset($_POST['insert'])){
    include_once 'connect.php';
  
    $pdo = connect();

    $nom_r = isset($_POST['nom_r']) ? $_POST['nom_r'] : '';
    $lien_r = isset($_POST['lien_r']) ? $_POST['lien_r'] : '';
    $date_r = ( isset($_POST['date_r']) and !empty($_POST['date_r']) ) ? $_POST['date_r'] : date('Y-m-d');
    $categorie = isset($_POST['categorie'])  ? $_POST['categorie'] : '';
    $image = isset($_POST['image_b']) ? $_POST['image_b'] : '';
    $description = isset($_POST['description']) ? $_POST['description'] : '';
    $tag = isset($_POST['tag']) ? $_POST['tag'] : '';
    //var_dump($tag);
    $x = 2;

    //recuperer id_categorie qui coresspond au nom de cat. $categorie de <select>
    $sql = "SELECT id_categorie FROM t_categorie WHERE nom='$categorie'";
    $res = $pdo->prepare($sql);
    $exec = $res->execute();
    $result = $res->fetch(PDO::FETCH_ASSOC);
    $id_cat = $result['id_categorie'];
    
    //echo "$categ";

    //recuperer id_tag qui coresspond au nom de tag  de <select>
    $sql = "SELECT id_tag FROM t_tag WHERE nom='$tag'";
    $res = $pdo->prepare($sql);
    $exec = $res->execute();
    $result = $res->fetch(PDO::FETCH_ASSOC);
    $id_tag = $result['id_tag'];
    //var_dump($id_tag);
    
    //var_dump($pdo);

    //on insere les données du form dans table t_ressource
    $sql = 'INSERT INTO `t_ressource`(`nom_ressource`, `lien`,`date_r`,`Image`, `description`, `id_user` ) VALUES (:nom_r,:lien_r, :date_r, :image_b, :description_r, :id_user)';
    $res = $pdo->prepare($sql);
    
    //print_r($res->errorInfo());
    try {
      $exec = $res->execute(array(":nom_r"=>$nom_r,":lien_r"=>$lien_r, ":date_r"=>$date_r, ":image_b"=>$image, ":description_r"=>$description, ":id_user"=>$x));
      
    } catch (PDOException $e) {
        echo 'Échec lors de la connexion : ' . $e->getMessage();
    }

    //var_dump($exec);
    
    // vérifier si la requête d'insertion a réussi
    if($exec){
      echo '<br>Données insérées dans t_ressource<br>';
    }else{
      echo 'Échec de l\'opération d\'insertion<br>';
    }

    $id_ressource = $pdo->lastInsertId(); //on recupere le id_ressource du dérnier enregistrement dans t_ressource

    //on insere les id_categorie et id_ressource dans t_cat_a_ress (la categorie de la ressource) pour enregistré la cat.
    $sql = "INSERT INTO `t_cat_a_ress`(`id_categorie`,`id_ressource` ) VALUES (:id_cat, :id_ress)";
    $res = $pdo->prepare($sql);
    $exec = $res->execute(array(":id_cat"=>$id_cat,":id_ress"=>$id_ressource));
    // vérifier si la requête d'insertion a réussi
    if($exec){
      echo 'Données insérées dans t_cat_a_ress<br>';
    }
    else{
      echo 'Échec de l\'opération d\'insertion<br>';
    }

    //on insere les id_tag et id_ressource dans t_ress_a_tag (le tag de la ressource) pour enregistré le tag
    $sql = "INSERT INTO `t_ress_a_tag`(`id_tag`,`id_ressource` ) VALUES (:id_tag, :id_ress)";
    $res = $pdo->prepare($sql);
    $exec = $res->execute(array(":id_tag"=>$id_tag,":id_ress"=>$id_ressource));
    // vérifier si la requête d'insertion a réussi
    if($exec){
      echo 'Données insérées dans t_ress_a_tag<br>';
    }
    else{
      echo 'Échec de l\'opération d\'insertion<br>';
    }
  
}



header("Refresh: 2;URL=accueil");

