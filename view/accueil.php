<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercice php mysql</title>
    <link rel="stylesheet" href="<?php echo ROOTSERVER; ?>/view/accueil.css">
</head>

<body>

<?php
   include_once 'php/ressource.model.php';
   include_once 'php/connect.php';
   $pdo = connect();

   $totalRess = countAllRess();
?>

<h1>Bookmarks<h1>
<div class="form_inputs">
    <form action="inserer" method="post">
    <div class="form_left">
        <div class="total" >Nombre total de bookmarks : <?= $totalRess; ?></div>
        Nom Bookmark: <input type="text" name="nom_r" /> 
        Lien Bookmark: <input type="text" name="lien_r" /> 
        Date de la ressource: <input type="text" name="date_r"/>
        Image Bookmark: <input type="text" name="image_b" />
    </div>
    <div class="form_right"> 
        <br>
        Categorie:
        <select name="categorie" value="categorie" class="select">  
        <?php  
            getCatForSelect();
        ?>
        </select>
        <br>
        
        Tag:
        <select name="tag" value="tag">  
        <?php  
            getTagForSelect();
        ?>
        </select>


        <p>Description: </p><textarea name="description" rows="5" cols="33"></textarea>
        <input class="submit" type="submit" name="insert" value="Insérer" />     

    </div>

    <br>
    </form>

    
    <a class="afficher" href="<?php echo ROOTSERVER; ?>/afficher"> Afficher et Modifier les Bookmarks</a>
    

</div>

</body>

</html>